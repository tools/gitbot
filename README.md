# Gitbot

A gitlab webhook to XMPP MUC bot.

Gitbot includes a web server called by the gitlab weebhook and an xmpp client that publishes the received weebhook as a text message in an xmpp room.

We have a gitbot instance that tracks the main repositories in the duniter gitlab and publishes to the [gitbot@muc.duniter.org](xmpp:gitbot@muc.duniter.org?join) room, but you can deploy your own gitbot instance to track repositories on any gitlab instance.

## Deploy a gitbot instance

Use docker image `duniter/gitbot:master`.

The configuration is done entirely by environment variables:

| Environment variable           | Default value | description                                                              |
|--------------------------------|---------------|--------------------------------------------------------------------------|
| `GITBOT_XMPP_JID`              | Mandatory     | Gitbot xmpp account full JID (user@host)                                 |
| `GITBOT_XMPP_PASSWORD`         | Mandatory     | Gitbot xmpp account password                                             |
| `GITBOT_XMPP_MUC`              | Mandatory     | Full jib of target xmpp room (room@host)                                 |
| `GITBOT_IP`                    | `0.0.0.0`     | IP address on which the gitbot server is listening (to receive webwook)  |
| `GITBOT_PORT`                  | `8080`        | port number on which the gitbot server is listening (to receive webwook) |
| `GITLAB_TOKEN`                 | Mandatory     | The secret token that gitlab must use for the webhook                    |
| `GITBOT_EXCLUDED_REPOSITORIES` | `""`          | List of git repositories that should not be tracked                      |
