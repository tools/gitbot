FROM debian:bookworm-slim

LABEL maintainer="elois <elois@duniter.org>"
LABEL version="0.1.0"
LABEL description="Gitlab webhook to XMPP MUC bot"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update && \
    apt-get -y install libssl-dev libssl3 ssl-cert ca-certificates openssl ca-certificates-java


RUN adduser --home /var/lib/gitbot gitbot

# Configuration
EXPOSE 8080
VOLUME /var/lib/gitbot
USER gitbot

# Intall
COPY ./build/gitbot /usr/local/bin/gitbot

CMD ["./usr/local/bin/gitbot"]
