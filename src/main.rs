use bytes::Bytes;
use gitlab::webhooks::{IssueAction, MergeRequestAction, WebHook, WikiPageAction};
use log::info;
use std::{
    collections::{HashMap, HashSet},
    net::{IpAddr, SocketAddr},
    str::FromStr,
    time::Duration,
};
use structopt::StructOpt;
use tokio::{sync::mpsc::UnboundedSender, time::Instant};
use warp::Filter as _;
use xmpp_parsers::Jid;

mod webhook;
mod webserver;
mod xmpp_;

#[derive(StructOpt, Debug)]
#[structopt(name = "Webhook to XMPP MUC bot")]
struct Opt {
    /// Jabber-ID (bare or full)
    #[structopt(short, long, env("GITBOT_XMPP_JID"))]
    jid: String,
    /// Jabber password
    #[structopt(short, long, env("GITBOT_XMPP_PASSWORD"))]
    password: String,
    /// Full MUC Jabber-Id, ie. room@conference.example.com/BotNickName
    #[structopt(short, long, env("GITBOT_XMPP_MUC"))]
    muc: String,
    /// Listen on 0.0.0.0 (IPv4) for HTTP instead of :: (IPv6)
    #[structopt(long, default_value = "0.0.0.0", env("GITBOT_IP"))]
    ip: IpAddr,
    /// HTTP listening port
    #[structopt(short = "P", long, default_value = "8080", env("GITBOT_PORT"))]
    port: u16,
    /// Gitlab token ("X-Gitlab-Token")
    #[structopt(short = "H", long = "header", env("GITLAB_TOKEN"))]
    gitlab_token: String,
    /// List of git repositories that should not be tracked
    #[structopt(short, long, env("GITBOT_EXCLUDED_REPOSITORIES"))]
    excluded_repositories: Vec<String>,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let jid = opt.jid;
    let password = opt.password;
    let muc_jid = opt.muc;
    let ip = opt.ip;
    let port = opt.port;
    let gitlab_token = opt.gitlab_token;
    let excluded_repositories = opt.excluded_repositories.into_iter().collect();

    pretty_env_logger::init();

    let (sender, mut receiver) = tokio::sync::mpsc::unbounded_channel();

    // Start webserver
    tokio::spawn(
        async move { webserver::start(SocketAddr::new(ip, port), gitlab_token, sender).await },
    );

    // Connect to xmpp server
    let mut xmpp_agent = xmpp_::XmppAgent::connect(&jid, &password, &[&muc_jid])
        .await
        .map_err(|e| anyhow::anyhow!("XMPP ERROR: {}", e))?;

    // Functional logic: convert each received webhook to text message and send it to xmpp room
    while let Some(gitlab_web_hook) = receiver.recv().await {
        if let Some(message) = webhook::format_webhook(&excluded_repositories, &gitlab_web_hook) {
            info!("message: {}", message);
            xmpp_agent
                .send_message(&message, &muc_jid)
                .await
                .map_err(|e| anyhow::anyhow!("XMPP ERROR: {}", e))?;
        } else {
            info!("Ignored webhook: {:?}", gitlab_web_hook);
        }
    }

    Ok(())
}
