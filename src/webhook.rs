use crate::*;

pub(super) fn format_webhook(
    excluded_repositories: &HashSet<String>,
    wh: &WebHook,
) -> Option<String> {
    log::debug!("receive webhook: {:?}", wh);
    Some(match wh {
        WebHook::Build(build) => {
            info!("Build: {:?}", build);
            return None;
        }
        WebHook::Issue(issue) => {
            if excluded_repositories.contains(&issue.project.path_with_namespace) {
                return None;
            }
            let action = match issue.object_attributes.action {
                Some(IssueAction::Update) => "updated",
                Some(IssueAction::Open) => "opened",
                Some(IssueAction::Close) => "closed",
                Some(IssueAction::Reopen) => "reopened",
                None => return None,
            };
            format!(
                "{} {} issue {} in *{}*: {}{}",
                issue.user.name,
                action,
                issue.object_attributes.iid,
                issue.project.name,
                issue.object_attributes.title,
                issue
                    .object_attributes
                    .url
                    .as_ref()
                    .map(|url| format!(" <{}>", url))
                    .unwrap_or_default()
            )
        }
        WebHook::MergeRequest(merge_req) => {
            if excluded_repositories.contains(&merge_req.project.path_with_namespace) {
                return None;
            }
            let action = match merge_req.object_attributes.action {
                Some(MergeRequestAction::Approved) => "approved",
                Some(MergeRequestAction::Update) => "updated",
                Some(MergeRequestAction::Open) => "opened",
                Some(MergeRequestAction::Close) => "closed",
                Some(MergeRequestAction::Merge) => "merged",
                Some(MergeRequestAction::Reopen) => "reopened",
                Some(MergeRequestAction::Unapproved) => "unapproved",
                None => return None,
            };
            format!(
                "{} {} merge request !{} in *{}*: {}{}",
                merge_req.user.name,
                action,
                merge_req.object_attributes.iid,
                merge_req.project.name,
                merge_req.object_attributes.title,
                merge_req
                    .object_attributes
                    .url
                    .as_ref()
                    .map(|url| format!(" <{}>", url))
                    .unwrap_or_default()
            )
        }
        WebHook::Note(note) => {
            info!("Note: {:?}", note);
            return None;
        }
        WebHook::Pipeline(pipeline) => {
            info!("Pipeline: {:?}", pipeline);
            return None;
        }
        WebHook::Push(push) => {
            if excluded_repositories.contains(&push.project.path_with_namespace) {
                return None;
            }
            let branch_name = if push.ref_.starts_with("refs/heads/") {
                &push.ref_[11..]
            } else {
                &push.ref_
            };
            let mut text = format!(
                "{} pushed {} commits to *{}* branch {}",
                push.user_name,
                push.commits.len(),
                push.project.name,
                branch_name
            );
            for commit in &push.commits {
                if let Some(subject) = commit.message.lines().next() {
                    text = format!("{}\n• {} <{}>", text, subject, commit.url);
                }
            }
            text
        }
        WebHook::WikiPage(page) => {
            if excluded_repositories.contains(&page.project.path_with_namespace) {
                return None;
            }
            let action = match page.object_attributes.action {
                WikiPageAction::Update => "updated",
                WikiPageAction::Create => "created",
            };
            format!(
                "{} {} *{}* wiki page {} <{}>",
                page.user.name,
                action,
                page.project.name,
                page.object_attributes.title,
                page.object_attributes.url,
            )
        }
    })
}
