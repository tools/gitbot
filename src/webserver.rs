use crate::*;

static OK_STR: &[u8] = b"Ok";

pub struct BadRequest(pub anyhow::Error);

impl std::fmt::Debug for BadRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl warp::reject::Reject for BadRequest {}

pub async fn start(addr: SocketAddr, gitlab_token: String, sender: UnboundedSender<WebHook>) {
    warp::serve(webhook_route(gitlab_token, sender))
        .run(addr)
        .await;
}

fn webhook_route(
    gitlab_token: String,
    sender: UnboundedSender<WebHook>,
) -> impl warp::Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::path::end()
        .and(warp::header::optional::<String>("content-type"))
        .and(warp::header::optional::<String>("X-Gitlab-Token"))
        .and(warp::body::bytes())
        .and(warp::any().map(move || gitlab_token.clone()))
        .and(warp::any().map(move || sender.clone()))
        .and_then(
            |content_type_opt: Option<String>,
             gitlab_token_opt: Option<String>,
             body: Bytes,
             expected_gitlab_token: String,
             sender: UnboundedSender<WebHook>| async move {
                if content_type_opt.as_deref() == Some("application/json") {
                    if gitlab_token_opt == Some(expected_gitlab_token) {
                        if let Ok(body_str) = std::str::from_utf8(body.as_ref()) {
                            if let Ok(gitlab_web_hook) = serde_json::from_str(body_str) {
                                if sender.send(gitlab_web_hook).is_ok() {
                                    Ok(warp::http::Response::new(OK_STR))
                                } else {
                                    Err(warp::reject::custom(BadRequest(anyhow::Error::msg(
                                        "Fail to send gitlab webhook",
                                    ))))
                                }
                            } else {
                                Err(warp::reject::custom(BadRequest(anyhow::Error::msg(
                                    "Fail to deserialize gitlab webhook",
                                ))))
                            }
                        } else {
                            Err(warp::reject::custom(BadRequest(anyhow::Error::msg(
                                "Body must be a valid utf8 string",
                            ))))
                        }
                    } else {
                        Err(warp::reject::custom(BadRequest(anyhow::Error::msg(
                            "Irvalid gitlab token",
                        ))))
                    }
                } else {
                    Err(warp::reject::custom(BadRequest(anyhow::Error::msg(
                        "No JSON submitted",
                    ))))
                }
            },
        )
}
