use crate::*;
use xmpp::{ClientBuilder, ClientType, Event};
use xmpp_parsers::{message::MessageType, BareJid};

const RECONNECTION_INTERVAL: u64 = 1_800;

pub(super) struct XmppAgent {
    agent: xmpp::Agent,
    jid: String,
    last_connection: Instant,
    password: String,
    rooms: HashMap<String, Jid>,
}

impl XmppAgent {
    pub async fn connect(jid: &str, password: &str, rooms: &[&str]) -> Result<Self, xmpp::Error> {
        let mut rooms_map = HashMap::with_capacity(rooms.len());
        for room in rooms {
            let room_jid = BareJid::from_str(room)?;
            rooms_map.insert((*room).to_owned(), Jid::Bare(room_jid.clone()));
        }

        let agent = Self::connect_inner(jid, password, &rooms_map, true).await?;

        Ok(Self {
            agent,
            jid: jid.to_owned(),
            password: password.to_owned(),
            last_connection: Instant::now(),
            rooms: rooms_map,
        })
    }
    pub async fn send_message(&mut self, message: &str, room: &str) -> Result<(), xmpp::Error> {
        if self.last_connection.elapsed() > Duration::from_secs(RECONNECTION_INTERVAL) {
            self.agent = Self::connect_inner(&self.jid, &self.password, &self.rooms, false).await?;
        }
        if let Some(room_jid) = self.rooms.get(room) {
            self.agent
                .send_message(room_jid.to_owned(), MessageType::Groupchat, "en", message)
                .await
        }
        Ok(())
    }
    async fn connect_inner(
        jid: &str,
        password: &str,
        rooms: &HashMap<String, Jid>,
        send_start_message: bool,
    ) -> Result<xmpp::Agent, xmpp::Error> {
        let mut agent = ClientBuilder::new(jid, password)
            .set_client(ClientType::Bot, "gitbot")
            .set_website("https://git.duniter.org/tools/gitbot")
            .set_default_nick("gitbot")
            .build()?;

        let mut rooms_joined = 0;
        'outer: while let Some(events) = agent.wait_for_events().await {
            for event in events {
                if let Event::Online = event {
                    info!("XMPP client now online at {}", jid);
                    for room_jid in rooms.values() {
                        agent
                            .join_room(
                                room_jid.to_owned().into(),
                                None,
                                None,
                                "en",
                                "Your friendly hook bot.",
                            )
                            .await;
                    }
                } else if let Event::RoomJoined(jid) = event {
                    info!("Entered MUC {}", jid);
                    if send_start_message {
                        agent
                            .send_message(
                                Jid::Bare(jid),
                                MessageType::Groupchat,
                                "en",
                                "Gitbot started",
                            )
                            .await;
                    }
                    rooms_joined += 1;
                    if rooms_joined == rooms.len() {
                        break 'outer;
                    }
                }
            }
        }

        Ok(agent)
    }
}
